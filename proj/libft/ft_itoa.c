/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 16:30:30 by vbudnik           #+#    #+#             */
/*   Updated: 2017/11/21 19:38:03 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_find_int(int n)
{
	int		j;
	int		div;
	int		res;

	res = n;
	div = 1;
	j = 1;
	while (res > 1 || res < -1)
	{
		res = n / (div * 10);
		div = div * 10;
		j++;
	}
	if (n >= -9 && n <= 9)
		j = 1;
	else
		j--;
	return (j);
}

static char		*ft_positive(int n, char *str)
{
	int i;

	i = 0;
	while (n != 0)
	{
		str[i] = (n % 10) + 48;
		n = n / 10;
		i++;
	}
	str[i] = '\0';
	return (str);
}

static char		*ft_negative(int n, char *str)
{
	int i;

	i = 0;
	while (n != 0)
	{
		str[i] = ((n % 10) * -1) + 48;
		n = n / 10;
		i++;
	}
	str[i] = '-';
	str[i + 1] = '\0';
	return (str);
}

static char		*ft_reverse(char *str)
{
	int		i;
	int		j;
	char	*strrev;

	j = 0;
	i = ft_strlen(str);
	if (!(strrev = (char *)malloc(sizeof(char) * i + 1)))
		return (NULL);
	while (str[j] != '\0')
	{
		strrev[j] = str[j];
		j++;
	}
	j = 0;
	i = ft_strlen(str) - 1;
	while (i >= 0)
	{
		str[i] = strrev[j];
		i--;
		j++;
	}
	return (str);
}

char			*ft_itoa(int n)
{
	char	*str;
	int		l;

	if (n == 0)
		return (ft_strdup("0"));
	l = ft_find_int(n);
	if (n < 0)
		l = l + 1;
	if (!(str = (char *)malloc(sizeof(char) * l + 1)))
		return (NULL);
	if (n < 0)
		str = ft_negative(n, str);
	else
		str = ft_positive(n, str);
	str = ft_reverse(str);
	return (str);
}
