void	copy_array(char **ar1, char **ar2)
{
	int		i;
	int		len;
	int		j;
	char	**tmp;

	i = 0;
	j = 0;
	if (!(tmp = (char **)malloc(sizeof(char *) * (ft_len2darr(ar1) + ft_len2darr(ar2) + 1))))
	{
		printf("Problem with malloc in copy array\n");
		exit (1);
	}
	while (ar2[i] != NULL)
	{
		tmp[j] = strdup(ar2[i]);
		free(ar2[i]);
		i++;
		j++;
	}
	free(ar2);
	ar2 = NULL;
	i = 0;
	while (ar1[i] != NULL)
	{
		tmp[j] = strdup(ar1[i]);
		free(ar1);
		i++;
		j++;
	}
	free(ar1);
	ar1 = NULL;
	tmp[j] = NULL;
	ar2 = (char **)malloc(sizeof(char *) * (j + 1));
	i = 0;
	while (tmp[i] != NULL)
	{
		ar2[i] = strdup(tmp[i]);
		free(tmp[i]);
		i++;
	}
	free(tmp);
	tmp = NULL;
	ar[i] = NULL;
}
