
#include "../inc/hitbtc.h"

void  write_curl_res_prev(char *str);
void  write_curl_res(char *str);

void	record(t_json *json, int r)
{
	int		i;
	int		b;
	int		a;
	int		s;
	int		c;
	int		o;
	int 	l;
	int		len;

	i = 0;
	a = 0;
	s = 0;
	o = 0;
	l = 0;
	len = json->len / 10;
	if (!(json->ask = (char **)malloc(sizeof(char *) * (len + 1))))
	{
		printf("PROBLEM with >ask\n");
		sleep(10);
		exit(1);
	}
	// if (r == 0)
	// {
		if (!(json->open = (char **)malloc(sizeof(char *) * (len + 1))))
		{
			printf("PROBLEM with open\n");
			sleep(10);
			exit(1);
		}
		// }
	if (!(json->last = (char **)malloc(sizeof(char *) * (len + 1))))
	{
		printf("PROBLEM with last\n");
		sleep(10);
		exit(1);
	}
	if (!(json->symbol = (char **)malloc(sizeof(char *) * (len + 1))))
		{
			printf("PROBLEM with symbol\n");
			sleep(10);
			exit(1);
		}
	while (i < json->len)
	{
		if (strstr(json->json_tokin[i], "ask") != NULL)
		{
			json->ask[a] = strdup(json->json_tokin[i]);
			a++;
		}
		if (strstr(json->json_tokin[i], "open") != NULL)
		// if (strstr(json->json_tokin[i], "open") != NULL && r == 0)
		{
			json->open[o] = strdup(json->json_tokin[i]);
			o++;
		}
		if (strstr(json->json_tokin[i], "last") != NULL)
		{
			json->last[l] = strdup(json->json_tokin[i]);
			l++;
		}
		if (strstr(json->json_tokin[i], "symbol") != NULL)
		{
			json->symbol[s] = strdup(json->json_tokin[i]);
			s++;
		}
		free(json->json_tokin[i]);
		i++;
	}
  free(json->json_tokin);
  json->json_tokin = NULL;
  json->ask[a] = NULL;
	// if (r == 0)
		json->open[o] = NULL;
	json->last[l] = NULL;
	json->symbol[s] = NULL;
}

void	corect_ask(t_json *json, int count)
{

	int		i;
	int		j;
	int		k;
	int		v;
	int		len;
	char	*tmp;

	if (count)
		free(json->ask_value);
	len = json->len / 10;
	json->ask_value = (double *)malloc(sizeof(double) * len);
	i = 0;
	v = 0;
	j = 0;
	while (i < len)
	{
		j = 0;
		if (json->ask[i] != NULL)
		{
			while (j < strlen(json->ask[i]))
			{
				if (isdigit((int)json->ask[i][j]))
				{
					tmp = ft_memalloc(strlen(json->ask[i]));
					k = 0;
					while ((isdigit((int)json->ask[i][j]) && json->ask[i][j] != '\0') ||
						   (json->ask[i][j] == '-' || json->ask[i][j] == '.'))
					{
						tmp[k] = json->ask[i][j];
						k++;
						j++;
					}
					json->ask_value[v]  = atof(tmp);
					v++;
					free(tmp);
				}
				if (json->ask[i][j] == 'n')
				{
					json->ask_value[v] = 0;
					v++;
					break ;
				}
				else
					j++;
			}
			free(json->ask[i]);
			i++;
		}
		else
		{
			json->ask_value[v] = 0;
			i++;
		}
	}
	free(json->ask);
	json->ask = NULL;
	json->ask_value[v] = 0;
}

void	corect_open(t_json *json, int count)
{

	int		i;
	int		j;
	int		k;
	int		v;
	int		len;
	char	*tmp;

	if (count)
		free(json->open_value);
	len = json->len / 10;
	json->open_value = (double *)malloc(sizeof(double) * len);
	i = 0;
	v = 0;
	j = 0;
	while (i < len)
	{
		j = 0;
		while (j < strlen(json->open[i]))
		{
			if (isdigit((int)json->open[i][j]))
			{
				tmp = ft_memalloc(strlen(json->open[i]));
				k = 0;
				while ((isdigit((int)json->open[i][j]) && json->open[i][j] != '\0') ||
					   (json->open[i][j] == '-' || json->open[i][j] == '.'))
				{
					tmp[k] = json->open[i][j];
					k++;
					j++;
				}
				json->open_value[v]  = atof(tmp);
				v++;
				free(tmp);
			}
			if ((json->open[i][j] == ':' && json->open[i][j + 1] == 'n') || json->open[i][j] == '-')
			{
				json->open_value[v] = 0;
				v++;
				break ;
			}
			else
				j++;
		}
		free(json->open[i]);
		i++;
	}
	free(json->open);
	json->open = NULL;
	json->open_value[v] = 0;
}

void	corect_last(t_json *json, int c)
{

	int		i;
	int		j;
	int		k;
	int		v;
	int		len;
	char	*tmp;

	if (c)
		free(json->last_value);
	len = json->len / 10;
	json->last_value = (double *)malloc(sizeof(double) * len);
	i = 0;
	v = 0;
	j = 0;
	while (i < len)
	{
		j = 0;
		while (j < strlen(json->last[i]))
		{
			if (isdigit((int)json->last[i][j]))
			{
				tmp = ft_memalloc(strlen(json->last[i]));
				k = 0;
				while ((isdigit((int)json->last[i][j]) && json->last[i][j] != '\0') ||
					   (json->last[i][j] == '-' || json->last[i][j] == '.'))
				{
					tmp[k] = json->last[i][j];
					k++;
					j++;
				}
				json->last_value[v]  = atof(tmp);
				v++;
				free(tmp);
			}
			if ((json->last[i][j] == ':' && json->last[i][j + 1] == 'n') || json->last[i][j] == '-')
			{
				json->last_value[v] = 0;
				v++;
				break ;
			}
			else
				j++;
		}
		free(json->last[i]);
		i++;
	}
	free(json->last);
	json->last = NULL;
	json->last_value[v] = 0;
}

int 	corect_symbol(t_json *json, int c)
{
	int		i;
	int		j;
	int		k;
	int		t;
	int		len;
	char	*tmp;

	if (c > 0)
	{
		if (ft_len2darr(json->symbol_name) != json->len / 10)
		{
			int ret;
			ret = ft_free_name(json->symbol_name, json->len_privius / 10);
			printf("\nPROBLEM WITH LEN of symbol name\n");
			printf("json->len / 10 == %d and ret == %d\n", json->len_privius / 10, ret);
			// write_curl_res(json->curl_res); // for writing array in file
			// write_curl_res_prev(json->curl_res_prev); // for write previon array in file
			write(1, "\a", 1);
			json->flag_leaks = 1;
			// sleep(10);
		}
		else
		{
			ft_freearr(json->symbol_name);
			free(json->symbol_name);
			json->symbol_name = NULL;
		}
	}
	len = json->len / 10;
	if (!(json->symbol_name = (char **)malloc(sizeof(char *) * (len + 1))))
	{
		printf("PROBLEM with symbol_name\n");
		// sleep(10);
		exit(1);
	}
	i = 0;
	k = 0;
	while (json->symbol[i] != NULL)
	{
		j = 0;
		while (json->symbol[i][j] != '\0')
		{
			if (json->symbol[i][j] >= 'A' && json->symbol[i][j] <= 'Z')
			{
				t = 0;
				tmp = ft_memalloc(ft_strlen(json->symbol[i]) - j + 1);
				while ((json->symbol[i][j] >= 'A' && json->symbol[i][j] <= 'Z') ||
					(json->symbol[i][j] >= '0' && json->symbol[i][j] <= '9') ||
				(json->symbol[i][j] >= 'a' && json->symbol[i][j] <= 'z'))
				{
					tmp[t] = json->symbol[i][j];
					t++;
					j++;
				}
				json->symbol_name[k] = strdup(tmp);
				if (json->symbol_name[k][0] == '\0')
					{
						printf("%s\n", json->symbol[i]);
						write(1, "\a", 1);
						sleep(20);
						free(json->symbol_name[k]);
						json->symbol_name[k] = strdup("lol");
					}
				free(tmp);
				k++;
			}
			else
				j++;
		}
		free(json->symbol[i]);
		i++;
	}
	free(json->symbol);
	json->symbol = NULL;
	json->symbol_name[k] = NULL;
	return (k);
}

void	swap(double **t1, double **t2)
{
	double *tmp;
	tmp  = *t1;
	*t1 = *t2;
	*t2 = tmp;
}


void	init_string(t_curl *s)
{
	s->len = 0;
	if (!(s->str = (char *)malloc(sizeof(char) * (s->len + 1))))
		exit(0);
	s->str[0] = '\0';
}

size_t	writefunc(void *ptr, size_t size, size_t nmemb, t_curl *s)
{
	size_t	new_len;

	new_len = s->len + size * nmemb;
	if (!(s->str = realloc(s->str, new_len)))
		exit(0);
	memcpy(s->str + s->len, ptr, size * nmemb);
	s->str[new_len] = '\0';
	s->len = new_len;
	return (size * nmemb);
}

void	get_curl(t_curl s, char **curl_res, char *str)
{
	CURL		*curl;
	CURLcode	res;

	curl = curl_easy_init();
	if (curl)
	{
		init_string(&s);
		curl_easy_setopt(curl, CURLOPT_URL, str);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
		res = curl_easy_perform(curl);
		curl_res[0] = strdup(s.str);
		free(s.str);
		curl_easy_cleanup(curl);
	}
}

char		**copy_array_glob(char **ar1, char **ar2)
{
	int		i;
	int		len;
	int		j;
	char	**tmp;

	i = 0;
	j = 0;
	if (!(tmp = (char **)malloc(sizeof(char *) * (ft_len2darr(ar1) + ft_len2darr(ar2) + 1))))
	{
		printf("Problem with malloc in copy array\n");
		exit (1);
	}
	while (ar2[i] != NULL)
	{
		tmp[j] = strdup(ar2[i]);
		free(ar2[i]);
		i++;
		j++;
	}
	free(ar2);
	ar2 = NULL;
	i = 0;
	while (ar1[i] != NULL)
	{
		tmp[j] = strdup(ar1[i]);
		i++;
		j++;
	}
	tmp[j] = NULL;
	return (tmp);
}

int		ft_len2darr(char **arr)
{
	int i;

	i = 0;
	while (arr[i] != NULL)
		i++;
	return (i);
}

void  write_curl_res(char *str)
{
  int   fd;

	if (!(fd = open("history/curl_res", O_WRONLY | O_CREAT)))
	{
	  printf("Can't opne file for first part\n");
	  exit (0);
	}
	write(fd, ",", 1);
	write(fd, str, strlen(str));
	close(fd);
}

void  write_curl_res_prev(char *str)
{
  int   fd;

if (!(fd = open("history/curl_res_prev", O_WRONLY | O_CREAT)))
{
  printf("Can't opne file for first part\n");
  exit (0);
}
  write(fd, ",", 1);
  write(fd, str, strlen(str));
  close(fd);
}

char	*get_name(void)
{
	uid_t			uid;
	struct passwd	*pw;

	uid = getuid();
	pw = getpwuid(uid);
	if (pw)
		return (pw->pw_name);
	return (NULL);
}
