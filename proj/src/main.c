
#include "../inc/hitbtc.h"

int		main(int ac, char **av)
{
	t_curl					s;
	t_json					json;
	t_search_param	user;

	int		count = 0;
	int 	j;
	int 	flag = 0;
	int		flag_f;
	int		flag_s = 0;
	int		k;
	int		t;


	if (strcmp(get_name(), NAME))
	{
		printf("Нарушены права лицензионного использования\n");
		printf("Обратитесь к разработчику проекта\n");
		exit(0);
	}
	put_param(&user);
	json.len = 0;
	json.len_privius = 0;
	while (ac)
	{
		printf("%s", "\x1b[H\x1b[2J");
		flag_s = 0;
		flag_f = 0;
		json.len_privius = json.len;
		if (count == 1)
		{
			json.const_len = json.len;
			if (!(json.const_value = (double *)malloc(sizeof(double) * (json.len / 10 + 1))))
			{
				printf("Problem with malloc json.const_value\n");
				sleep(10);
				continue ;
			}
			if (!(json.const_symbol_name = (char **)malloc(sizeof(char *) * (json.len / 10 + 1))))
				exit(0);
			int k = 0;
			while (k < json.len / 10)
			{
				json.const_value[k] = json.last_value[k];
				k++;
			}
			int g = 0;
			while (g < json.len / 10)
			{
				json.const_symbol_name[g] = strdup(json.symbol_name[g]);
				g++;
			}
			json.const_symbol_name[g] = NULL;
		}
		if (count > 1)
		{
			if (count > 2)
			{
				free(json.curl_res_prev);
				json.curl_res_prev = NULL;
			}
			if (count > 2)
			{
				ft_freearr(json.last_symbol);
				free(json.last_symbol);
				json.last_symbol = NULL;
			}
			json.last_symbol = (char **)malloc(sizeof(char *) *
								(json.len / 10 + 1));
			swap_symbol(&json);
			json.curl_res_prev = strdup(json.curl_res);
			free(json.curl_res);
			json.curl_res = NULL;
		}
		if (ac > 1 && strcmp(av[1], "test") == 0)
			test_secondpart(count, &json.curl_res);
		else
			get_curl(s, &json.curl_res, "https://api.hitbtc.com/api/2/public/ticker");
		if (strstr(json.curl_res, "502 Bad Gateway") != NULL)
		{
			printf("Problem with server\n");
			continue ;
		}
		json.json_tokin = ft_strsplit(json.curl_res, ',');
		if (json.json_tokin[0] == '\0')
		{
			printf("PROBLEM WITH SERWER CHAEK CONECT WITH INTERNE\n");
			sleep(10);
			continue ;
		}
		if (count == 0)
		{
			free(json.curl_res);
			json.curl_res = NULL;
		}
		json.len = ft_len2darr(json.json_tokin);
		record(&json, count);
		corect_ask(&json, count);
		// if (count == 0)
			corect_open(&json, count);
		corect_last(&json, count);
		if (corect_symbol(&json, count) != json.len / 10)
		{
			printf("ETO TUT CTOTO NE TO \n");
			sleep(10);
			exit(1);
		}
		if (count > 1)
		{
			// if (json.len == json.len_privius)
			// 	printf("%s\n", "all good");
			if (find_coint(&json))
			{
				printf("NEW COIN\n");
				usleep(3);
			}
		}
		if (count > 0)
			create_arrays(&json, count);
		if (count > 0)
		{
			k = 0;
			t = 0;
			j = 0;
			if (user.reg == 1 || user.reg == 3)
			{
				if (count > 1)
					free(json.change2);
				json.change2 = (double *)malloc(sizeof(double) * (json.len / 10 + 1));
			}
			if (user.reg == 2 || user.reg == 3)
			{
				if (count > 1)
					free(json.change);
				json.change = (double *)malloc(sizeof(double) * (json.len / 10 + 1));
			}
			while (j < json.len / 10 - 1)
			{
				if (user.reg == 1 || user.reg == 3)
				{
					flag = first_part(&json, j, &user, &k);
					if (flag == 1)
						flag_f += 1;
				}
				if (user.reg == 2 || user.reg == 3)
				{
					flag = second_part(&json, j, &user, &t, count);
					if (flag == 1)
						flag_s += 1;
				}
				j++;
			}
			json.f1_reset[k] = NULL;
			json.f2_reset[t] = NULL;
			// free_arr_json(&json);
		}
		if (flag_f || flag_s)
			beeper_ask(user.beep_time, flag_f, flag_s, &json, &user);
		if (count > 0)
		{
				ft_free_name(json.f1_reset, flag_f);
				free(json.f1_reset);
				json.f1_reset = NULL;
		}
		if (count > 0)
		{
				ft_free_name(json.f2_reset, flag_s);
				free(json.f2_reset);
				json.f2_reset = NULL;
		}
		if (count < 3)
			sleep(1);
		if (count > 3)
		{
			printf("COUTER +++ %d \n", count);
			sleep(user.time);
		}
		count++;
	}
	return (0);
}

void	create_arrays(t_json *json, int c)
{
	// if (c > 1)
	// {
	// 	if (json->f1_reset)
	// 	{
	// 		printf("a1");
	// 		free(json->f1_reset);
	// 		printf("a2");
	// 		json->f1_reset = NULL;
	// 		printf("a3");
	// 	}
	// 	if (json->f2_reset)
	// 	{
	// 		free(json->f2_reset);
	// 		printf("a4");
	// 		json->f2_reset = NULL;
	// 		printf("a5");
	// 	}
	// }
	if (!(json->f1_reset = (char **)malloc(sizeof(char *) * json->len / 10 + 1)))
	{
		printf("PROBLEM with json->f1_reset\n");
		sleep(10);
		create_arrays(json,c);
	}
	if (!(json->f2_reset = (char **)malloc(sizeof(char *) * json->len / 10 + 1)))
	{
		printf("PROBLEM with json->f1_reset\n");
		sleep(10);
		create_arrays(json,c);
	}
}
/*

INKUSD > 61248.0662983425
							0.0001810000 --- 0.1110400000 and j == 763


const == 0.0001810000 and last == 0.0000000000 and q == 763


INKUSD > 5551900.0000000000
							0.0000020000 --- 0.1110400000 and j == 764
const == 0.0000020000 and last == 0.0000020000 and q == 764

*/
