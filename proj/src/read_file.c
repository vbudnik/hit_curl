#include "../inc/hitbtc.h"
#include <fcntl.h>

char    **read_file_1(void)
{
  char  **res;
  char  *tmp;
  int   i;
  int   fd;

  i = 0;
if (access("history/first_part", F_OK) == -1)
   // if (access("../history/first_part", F_OK) == -1)
  {
    printf("Нет файла в папке history\n");
    printf("Создайте файла с именем --- first_part --- и заполните его\n");
    printf("для продолжения роботы \n");
    exit (0);
  }
if (!(fd = open("history/first_part", O_RDONLY)))
   // if (!(fd = open("../history/first_part", O_RDONLY)))
  {
    printf("Can't opne file for first part\n");
    exit (0);
  }
  get_next_line(fd, &tmp);
  res = ft_strsplit(tmp, ',');
  free(tmp);
  close(fd);
  return (res);
}

char    **read_file_2(void)
{
  char  **res;
  char  *tmp;
  int   i;
  int   fd;

  i = 0;
if (access("history/second_part", F_OK) == -1)
   // if (access("../history/second_part", F_OK) == -1)
  {
    printf("Нет файла в папке history\n");
    printf("Создайте файла с именем --- second_part --- и заполните его\n");
    printf("для продолжения роботы \n");
    exit (0);
  }
if (!(fd = open("history/second_part", O_RDONLY)))
   // if (!(fd = open("../history/second_part", O_RDONLY)))
  {
    printf("Can't opne file for second part\n");
    exit (0);
  }
  get_next_line(fd, &tmp);
  res = ft_strsplit(tmp, ',');
  free(tmp);
  close(fd);
  return (res);
}

void    write_file_1(char *str)
{
  int   fd;

if (access("history/first_part", F_OK) == -1)
   // if (access("../history/first_part", F_OK) == -1)
  {
    printf("Нет файла в папке history\n");
    printf("Создайте файла с именем --- first_part --- и заполните его\n");
    printf("для продолжения роботы \n");
    exit (0);
  }
if (!(fd = open("history/first_part", O_WRONLY | O_APPEND)))
   // if (!(fd = open("../history/first_part", O_WRONLY | O_APPEND)))
  {
    printf("Can't opne file for first part\n");
    exit (0);
  }
  write(fd, ",", 1);
  write(fd, str, strlen(str));
  close(fd);
}

void  write_file_2(char *str)
{
  int   fd;

if (access("history/second_part", F_OK) == -1)
   // if (access("../history/second_part", F_OK) == -1)
  {
    printf("Нет файла в папке history\n");
    printf("Создайте файла с именем --- second_part --- и заполните его\n");
    printf("для продолжения роботы \n");
    exit (0);
  }
if (!(fd = open("history/second_part", O_WRONLY | O_APPEND)))
   // if (!(fd = open("../history/second_part", O_WRONLY /*| O_APPEND*/)))
  {
    printf("Can't opne file for first part\n");
    exit (0);
  }
  write(fd, ",", 1);
  write(fd, str, strlen(str));
  close(fd);
}
